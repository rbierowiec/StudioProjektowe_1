# -*- coding: utf-8 -*-
import math,sys
class Interface():

    def errorReturn(self,line):
        print u"Błąd w linii nr: "+str(line+1)


    def readFromFile(self, filename, program):
        try:
            with open( filename + '.txt', "r") as f:
                wiersze = f.readlines()
                dane = []
                index=0
                for wiersz in wiersze:

                    if program == 'Program2' and index == 7:
                        break
                    value = wiersz[:].rstrip().split(':')[1].replace(' ','')
                    validate = self.ValidateNumber(value)
                    if validate == True: self.errorReturn(index); return


                    if index in (0,1,2):
                        if index == 0:
                            value = int(math.ceil(float(value) / 2))
                            if value % 2 == 1:
                                value += 1

                        value=int(float(value))
                        validate = self.ValidatePositive(value)
                        if validate == True: self.errorReturn(index); return

                    if index in (3,4):
                        value=int(float(value))
                        validate = self.ValidateSelection(value)
                        if validate == True: self.errorReturn(index); return

                    if index in (5,):
                        value=float(value)
                        validate = self.ValidateRange(value)
                        if validate == True: self.errorReturn(index); return

                    if index in (6,):
                        value = int(float(value))
                        validate = self.ValidateRange0to4(value)
                        if validate == True: self.errorReturn(index); return

                    if index in (7,8,9,10,11):
                        value = float(value)
                        validate = self.ValidateInteger(value)
                        if validate == True: self.errorReturn(index); return
                        value = int(value)

                    dane.append(value)
                    index+=1
            if program == 'Program2':
                for i in range(7,12):
                    dane.append(0)

            return dane
        except:
            print "Błąd - Plik nie istnieje"
            sys.exit()

    def ValidateInteger(self, number):
        if not (int(number) == number):
            print(u"Błąd - Podana wartość nie jest liczbą całkowitą")
            return True
        return False

    def ValidateNumber(self,number):
        try:
            float(number)
        except ValueError:
            print(u"Błąd - Podana wartość nie jest liczbą")
            return True
        return False

    def ValidatePositive(self,number):
        if int(number) <= 0:
            print u"Błąd - Wartość mniejsza lub równa 0"
            return True
        return False

    def ValidateSelection(self,number):
        if not(number==1 or number == 2):
            print u"Błąd - Wartość różna od 1 lub 2"
            return True
        return False

    def ValidateRange(self,number):
        if not (0.3 >= number >= 0):
            print u"Błąd - Wartość nie leży w przedziale [0;0.3]"
            return True
        return False

    def ValidateRange0to4(self,number):
        if number not in range(5):
            print u"Błąd - Wartość nie leży w zbiorze liczb całkowitych [0;4]"
            return True
        return False

    def TerminalInterface(self):

        print u"Podaj początkową liczbę rozwiązań w pierwszej generacji"
        while True:
            parentsNumber = raw_input()
            validate = self.ValidateNumber(parentsNumber)
            if validate: continue

            parentsNumber = int(float(parentsNumber))
            validate = self.ValidatePositive(parentsNumber)
            if validate: continue
            break
        parentsNumber = int(math.ceil(float(parentsNumber) / 2))

        if parentsNumber % 2 == 1:
            parentsNumber += 1

        print u"Podaj liczbę wezłów"
        while True:
            nodeNumber = raw_input()
            validate = self.ValidateNumber(nodeNumber)
            if validate: continue
            nodeNumber = int(float(nodeNumber))
            validate = self.ValidatePositive(nodeNumber)
            if validate: continue
            break

        print u"Podaj maksymalna liczbę iteracji"
        while True:
            iterationNumber = raw_input()
            validate = self.ValidateNumber(iterationNumber)
            if validate: continue
            iterationNumber = int(float(iterationNumber))
            validate = self.ValidatePositive(iterationNumber)
            if validate: continue
            break

        print u"Podaj metodę selekcji \n 1-elitarna \n 2-rankingowa"
        while True:
            selectionType=int(raw_input())
            validate = self.ValidateSelection(selectionType)
            if validate: continue
            break

        print u"Podaj metodę mutacji \n 1-równomierna \n 2-wewnętrzna"

        while True:
            mutationType = int(raw_input())
            validate = self.ValidateSelection(mutationType)
            if validate: continue
            break


        print u"Podaj prawdopodobieństwo mutacji <0;0,3>"
        while True:
            mutationProbability = raw_input()
            validate = self.ValidateNumber(mutationProbability)
            if validate: continue
            mutationProbability = float(mutationProbability)
            validate = self.ValidateRange(mutationProbability)
            if validate: continue
            break

        print u"Podaj maksymalną liczbę agregacji klasy"
        while True:
            agregations = raw_input()
            validate = self.ValidateNumber(agregations)
            if validate: continue
            agregations = int(float(agregations))
            validate = self.ValidateRange0to4(agregations)
            if validate: continue
            break

        return parentsNumber, nodeNumber, iterationNumber, mutationType, mutationProbability, selectionType, agregations

    def fileInterface(self,program):
        print u'Podaj nazwę pliku'
        filename = raw_input()
        interFace = Interface()
        print u'Wczytuję: '+ filename + '.txt'
        dane = interFace.readFromFile(filename,program)
        try:
            return dane[0], dane[1], dane[2], dane[3], dane[5], dane[4], dane[6], dane[7], dane[8], dane[9], dane[10], dane[11]
        except :
            if dane is not None:
                print(u"Błąd - Błędna struktura pliku")
            sys.exit()
