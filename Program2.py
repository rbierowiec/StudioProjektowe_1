# -*- coding: utf-8 -*-
from Interface.InterfaceFunction import Interface
from drowIGraph import classMatrixToDraw
from geneticAlgorythm.geneticManager import procesAlgorithm
from metricsCalculators.calculateDkSeriesMetrics import calculateDkSeriesMetrics

if __name__=='__main__':

    ##### Interface start #####
    print u'Wybierz interfejs \n1 - wartości z pliku\n2 - wartości ręczne'

    while True:
        interfacenr = raw_input()
        if interfacenr in ('1', '2'): break
        print "Liczba błędna"

        interfaceClass = Interface()
    if interfacenr == '2':
        parentsNumber, nodeNumber, iterationNumber, mutationType, mutationProbability, selectionType, agregations = interfaceClass.TerminalInterface()
    elif interfacenr == '1':
        parentsNumber, nodeNumber, iterationNumber, mutationType, mutationProbability, selectionType, agregations,  null, null, null, null, null = interfaceClass.fileInterface('Program2')

    ##### Find pattern #####

    metrics = {}
    metrics['dkseries'] = 1
    A = procesAlgorithm(parentsNumber, nodeNumber, 1, 'linear', mutationType, mutationProbability, selectionType, agregations, metrics)

    # A = procesAlgorithm(2, 4, 1, 'linear', 2, 0.00, 1, 2, metrics)

    print A.bestSolution
    print A.bestConnection
    print A.m2kAnd3kSeriesMetric

    classMatrixToDraw.MatrixToDraw(A.bestSolution,A.bestConnection)

    ##### Program start #####

    metrics['dkseries']=2
    # B = procesAlgorithm(80, 4, 40, 'linear', 2, 0.00, 1, 2, metrics, A.m2kAnd3kSeriesMetric)
    B = procesAlgorithm(parentsNumber, nodeNumber, iterationNumber, 'linear', mutationType, mutationProbability, selectionType, agregations, metrics, A.m2kAnd3kSeriesMetric)

    #####    Results    #####
    print "Najlepszy rezultat:",B.bestResult
    print "Najlepszy matrix: \n", B.bestSolution
    print B.bestConnection
    print "Znaleziono w iteracji: ", B.generationNumber
    # print A.m2kAnd3kSeriesMetric
    # print B.m2kAnd3kSeriesMetric
    metricsManagerInstance = calculateDkSeriesMetrics()
    m2kAnd3kSeriesMetric = metricsManagerInstance.prepare2kAnd3kSeriesMetric(B.bestSolution)
    print m2kAnd3kSeriesMetric

    classMatrixToDraw.MatrixToDraw(B.bestSolution, B.bestConnection)