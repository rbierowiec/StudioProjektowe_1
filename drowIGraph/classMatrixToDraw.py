#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import igraph as ig

class MatrixToDraw:

    def __init__(self,matrix,array):
        coloursEdge=[]
        coloursVec=[]
        graph=ig.Graph(directed=True)
        lengthMatrix=len(matrix[0])
        graph.add_vertices(lengthMatrix)
        for node in range(len(matrix)):
            for i in range(lengthMatrix):
                for j in range(lengthMatrix):
                    if (matrix[node][i][j]>0):
                        for vec in range(matrix[node][i][j]):
                            graph.add_edge(i,j)
                            if node == 0:
                                coloursVec.append("green")
                            elif node == 1:
                                coloursVec.append("orange")

        for edge in array:
            if edge == 0:
                coloursEdge.append('red') #klasa
            elif edge == 1:
                coloursEdge.append('aqua') #instancja
            else:
                coloursEdge.append('green') #property

        graph.es["color"] = coloursVec
        graph.vs["color"]=coloursEdge
        graph.vs["label"]=range(lengthMatrix)
        layout = graph.layout("fr")
        ig.plot(graph, layout=layout)





if __name__=='__main__':
    x=np.array([[[0, 0, 1],
  [1, 0, 1],
  [0, 0, 0]],

 [[1, 1, 2],
  [0, 0, 0],
  [1, 0, 0]]])
    y=[1,0,1]
    # print x[0,0,0]
    # print x
    MatrixToDraw(x,y)






# g = ig.Graph(directed=True)
#         g.add_vertices(4)
#         g.add_edges([(0, 1),(1,2),(0,0),(0,3),(3,1),(2,3),(3,0)])
#
#         g.degree(mode="in")  # -> [0, 1]
#         g.degree(mode="out")  # -> [1, 0]
#         print ig.summary(g)
#         g.vs["label"] = ["0", "1", "2", "3"]
#         g.vs["color"] = ["blue", "pink", "red", "green"]
#         g.es["color"] = ["blue", "pink", "red", "green", "yellow", "black", "gray"]
#         g.es["label"] = ["blue", "pink", "red", "green", "yellow", "black", "gray"]
#         g.es["name"] = ["blue", "pink", "red", "green", "yellow", "black", "gray"]
#         layout = g.layout("rt")
#         ig.plot(g, layout=layout)
#         print g.spanning_tree()
#         # print g.shortest_paths_dijkstra()