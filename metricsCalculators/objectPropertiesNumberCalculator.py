from graphOperations import graphOperations
import numpy as np

class objectPropertiesNumberCalculator:
    def calculateObjectPropertiesNumber(self, objectProperties, matrixSize):
        nubmerOfProperties = np.zeros(matrixSize, dtype='int')

        for nodeNumber in objectProperties:
            nubmerOfProperties[nodeNumber] = 0
            for propertyType in objectProperties[nodeNumber]:
                for propertyName in objectProperties[nodeNumber][propertyType]:
                    nubmerOfProperties[nodeNumber] += objectProperties[nodeNumber][propertyType][propertyName]

        return nubmerOfProperties