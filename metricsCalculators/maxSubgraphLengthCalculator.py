#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from graphOperations import graphOperations

class maxSubgraphLengthCalculator:
    def calculateMaxSubgraphLength(self, matrix):
        graphOperationsManager = graphOperations()

        maxSubgraphLength = 0
        verticesNumber = len(matrix[0])

        while maxSubgraphLength == 0:
            subgraphs = graphOperationsManager.getAllNVerticesPaths(verticesNumber, matrix, 1)
            
            if len(subgraphs) > 0 or verticesNumber == 1:
                return verticesNumber
            else:
                verticesNumber -= 1