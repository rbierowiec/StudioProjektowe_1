from graphOperations import graphOperations

class maxNodeDegreeCalculator:
    def calculateMaxDegree(self, matrix):
        graphOperationsManager = graphOperations()

        maxDegree = 0
        for x in range(0, len(matrix[0])):

            currentDegree = graphOperationsManager.calculateVerticeDegree(x, matrix)
            if currentDegree > maxDegree:
                maxDegree = currentDegree
            
        return maxDegree