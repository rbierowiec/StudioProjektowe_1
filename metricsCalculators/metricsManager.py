#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import time

from averageNodeDegreeCalculator     import averageNodeDegreeCalculator
from maxNodeDegreeCalculator         import maxNodeDegreeCalculator
from maxSubgraphLengthCalculator    import maxSubgraphLengthCalculator
from medianNodeDegreeCalculator      import medianNodeDegreeCalculator

class timeBuffor:
    averageNodeDegree = 0
    maxNodeDegree = 0
    maxSubgraphLength = 0
    medianDegree = 0
    avarageNodeDegree2 = 0
    avarageNodeDegree3 = 0

    @staticmethod
    def printDatas():
        print "averageNodeDegree: " + str(timeBuffor.averageNodeDegree)
        print "averageNodeDegreePart: " + str(timeBuffor.avarageNodeDegree2)
        print "averageNodeDegreePart2: " + str(timeBuffor.avarageNodeDegree3)
        print "maxNodeDegree: " + str(timeBuffor.maxNodeDegree)
        print "maxSubgraphLength: " + str(timeBuffor.maxSubgraphLength)
        print "medianDegree: " + str(timeBuffor.medianDegree)

    @staticmethod
    def add(timeCost):
        timeBuffor.avarageNodeDegree2 += timeCost

# Klasa metricsManager będzie wykorzystywana do liczenia metryk macierzy,
# będzie przyjmowała macierz jako parametr oraz wykorzystując inne klasy (liczące poszczególne metryki)
# zwracała policzone metryki w odpowiedniej tablicy

class metricsManager:
    def calculateMetrics(self, matrix, metricsAll=[]):
        averageNodeDegreeCalculatorInstance  = averageNodeDegreeCalculator()
        maxNodeDegreeCalculatorInstance      = maxNodeDegreeCalculator()
        maxSubgraphLengthCalculatorInstance = maxSubgraphLengthCalculator()
        medianNodeDegreeCalculatorInstance   = medianNodeDegreeCalculator()

        metrics = {}

        for metric in metricsAll:
            if metricsAll[metric]>0:
                if metric == 'avarageNodeDegree' :
                    for sumbetric in metricsAll[metric]:
                        # Licz metrykę avarageNodeDegree tylko wtedy, gdy nie została jeszcze policzona
                        try:
                            metrics['avarageNodeDegree']
                        except KeyError:
                            if metricsAll[metric][sumbetric] > 0:
                                start_time = time.time()
                                tmp = averageNodeDegreeCalculatorInstance.calculateSubgraphsAvergeNodeDegree(matrix)
                                metrics['avarageNodeDegree']     = tmp[0]
                                elapsed_time = time.time() - start_time
                                timeBuffor.avarageNodeDegree2 = tmp[1]
                                timeBuffor.avarageNodeDegree3 = tmp[2]
                                timeBuffor.averageNodeDegree += elapsed_time
                elif  metric == 'medianDegree':
                    start_time = time.time()
                    metrics['medianDegree']          = medianNodeDegreeCalculatorInstance.calculateMedianDegree(matrix)
                    elapsed_time = time.time() - start_time
                    timeBuffor.medianDegree += elapsed_time
                elif metric == 'maxNodeDegree':
                    start_time = time.time()
                    metrics['maxNodeDegree']         = medianNodeDegreeCalculatorInstance.returnMaxNodeDegree(matrix)
                    elapsed_time = time.time() - start_time
                    timeBuffor.maxNodeDegree += elapsed_time
                elif metric == 'maxSubgraphLength':
                    start_time = time.time()
                    metrics['maxSubgraphLength']    = maxSubgraphLengthCalculatorInstance.calculateMaxSubgraphLength(matrix)
                    elapsed_time = time.time() - start_time
                    timeBuffor.maxSubgraphLength += elapsed_time

        return metrics

    def printDetails(self):
        timeBuffor.printDatas()