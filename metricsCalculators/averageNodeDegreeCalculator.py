#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from graphOperations import graphOperations
import time, numpy

class averageNodeDegreeCalculator:
    timeCost = 0
    timeCost2 = 0

    nodeDegrees = {}
    nodeDegreesCalculated = 0

    # Funkcja liczy stopień dla każdego węzła grafu
    def calculateVerticeDegree(self, matrix):
        if self.nodeDegreesCalculated == 0:
            graphOperationsManager = graphOperations()

            for nodeNumber in range(len(matrix[0])):
                self.nodeDegrees[nodeNumber] = graphOperationsManager.calculateVerticeDegree(nodeNumber, matrix)
            self.nodeDegreesCalculated = 1

    def calculateAverageNodeDegree(self, matrix):
        numberOfNodes = len(matrix[0])
        numberOfEdge = sum(map(sum, matrix))
        
        averageNodeDegree = numberOfEdge / numberOfNodes

        averageNodeDegree = round(averageNodeDegree, 2)
        return averageNodeDegree

    def calculateSubgraphsAvergeNodeDegree(self, matrix):
        limit = 4
        if (len(matrix[0]) < limit):
            limit = len(matrix[0])

        graphOperationsManager = graphOperations()
        averageNodeDegree = {}
        
        self.calculateVerticeDegree(matrix)

        for i in range(2, limit):
            
            # start_time = time.time()
            subgraphs = graphOperationsManager.getAllNVerticesPaths(i, matrix, "*")
            # elapsed_time = time.time() - start_time
            # self.timeCost += elapsed_time

            if len(subgraphs) > 0:
                # start_time = time.time()
                if i == 2:
                    sumOfDegrees = numpy.sum([[self.nodeDegrees[x[0]],self.nodeDegrees[x[1]]] for x in subgraphs])
                elif i == 3:
                    sumOfDegrees = numpy.sum([[self.nodeDegrees[x[0]],self.nodeDegrees[x[1]],self.nodeDegrees[x[2]]] for x in subgraphs])
                
                # elapsed_time = time.time() - start_time
                # self.timeCost2 += elapsed_time
                averageNodeDegree[i] = round(sumOfDegrees / (len(subgraphs) * i), 2)

        return {0: averageNodeDegree, 1: self.timeCost, 2: self.timeCost2}