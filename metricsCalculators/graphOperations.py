#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import numpy, time
import networkx, itertools

class graphOperations:
    timeCost = 0
    # Funkcja przygotowuje mapę połączeń między węzłami (np czy węzeł 1 ma połączenie z węzłem 2)
    def prepareNeighborsMap(self, matrix):
        numberOfNodes = len(matrix[0])
        self.neighborsMap = numpy.zeros((numberOfNodes, numberOfNodes), dtype=bool)
        for node1 in range(numberOfNodes):
            for node2 in range(numberOfNodes):
                self.neighborsMap[node1][node2] = matrix[0][node1][node2] > 0 or matrix[0][node2][node1] > 0 or matrix[1][node1][node2] > 0 or matrix[1][node2][node1] > 0
    
    # Funkcja liczy stopień zadanego węzła w zadanej macierzy
    def calculateVerticeDegree(self, vNumber, matrix):
        vDegree = 0
        for node in range(len(matrix)):
            vDegree += numpy.sum([numpy.sum(matrix[node][vNumber]),numpy.sum(matrix[node][:][:, vNumber])])

        return vDegree
    # Funkcja wycina z całego grafu mniejszy podgraf (o zadanych wierzchołkach jako tablica vertices)
    def getPartOfGraph(self, vertices, matrix):
        resultMatrix = numpy.zeros((len(vertices),len(vertices)))
        indexX = 0
        for vertice in vertices:
            indexY = 0
            for vertice2 in vertices:
                if vertice != vertice2:
                    resultMatrix[indexX][indexY] = matrix[0][vertice][vertice2]
                indexY += 1
            indexX += 1
        
        return resultMatrix

    def calculateEdgeNumber(self, vertices, matrix):
        numberOfVertices = 0

        for node in range(len(matrix)):
            for vert in vertices:
                for vert2 in vertices:
                    numberOfVertices += matrix[node][vert][vert2]

        return numberOfVertices
    # Funkcja zwraca wszystkie n-węzłowe podgrafy grafu
    def getAllNVerticesPaths(self, n, matrix, lookingForNumber):
        def checkIfOk3(arrayOfNodes):
            return ((self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[1]] and self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[2]]) or (self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[1]] and self.neighborsMap[arrayOfNodes[1]][arrayOfNodes[2]]) or (self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[2]] and self.neighborsMap[arrayOfNodes[1]][arrayOfNodes[2]]))
        
        try:
            self.neighborsMap
        except:
            self.prepareNeighborsMap(matrix)
        resultArray = []

        # Generujemy wszystkie możliwe kombinacje węzłów n-elementowych
        subGraphs = itertools.combinations(range(len(matrix[0])), n)

        # W pętli sprawdzamy, czy nasza kombinacja n-elementowa posiada połączenia między węzłami
        if n == 2:
            resultArray = [arrayOfNodes for arrayOfNodes in subGraphs if self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[1]]]
        elif n == 3:
            resultArray = [arrayOfNodes for arrayOfNodes in subGraphs if ((self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[1]] and self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[2]]) or (self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[1]] and self.neighborsMap[arrayOfNodes[1]][arrayOfNodes[2]]) or (self.neighborsMap[arrayOfNodes[0]][arrayOfNodes[2]] and self.neighborsMap[arrayOfNodes[1]][arrayOfNodes[2]]))]

        return resultArray