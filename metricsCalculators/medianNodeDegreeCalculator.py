from graphOperations import graphOperations
import numpy

class medianNodeDegreeCalculator: 
    medianDegree = 0
    maxDegree = 0
    metricsAlreadyCalculated = 0

    def calculateMetrics(self, matrix):
        if self.metricsAlreadyCalculated == 0:
            graphOperationsManager = graphOperations()
            degreesArray = []

            for item in range(len(matrix[0])):
                degreesArray.append(graphOperationsManager.calculateVerticeDegree(item, matrix))

            degreesArray.sort()
            self.medianDegree = numpy.median(degreesArray)
            self.maxDegree = numpy.amax(degreesArray)
            self.metricsAlreadyCalculated = 1

    def calculateMedianDegree(self, matrix):
        self.calculateMetrics(matrix)
        return self.medianDegree

    def returnMaxNodeDegree(self, matrix):
        self.calculateMetrics(matrix)
        return self.maxDegree