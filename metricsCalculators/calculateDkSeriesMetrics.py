#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import os, sys, inspect, numpy, json

dictionary = ['..\drowIGraph', '..\metricsCalculators']
for dict in dictionary:     
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],dict)))     
    if cmd_subfolder not in sys.path:         
        sys.path.insert(0, cmd_subfolder)

from graphOperations import graphOperations
from classMatrixToDraw import MatrixToDraw
from maxSubgraphLengthCalculator import maxSubgraphLengthCalculator

class calculateDkSeriesMetrics:
    def calculateDkSeries(self, matrix, nodes):
        graphOperationsManager = graphOperations()
        subgraphs = graphOperationsManager.getAllNVerticesPaths(nodes, matrix, "*")
        
        metric = {}
        metric['low'] = 0
        metric['medium'] = 0
        metric['high'] = 0

        maxVerticeDegree = -1
        minVerticeDegree = 999999999

        verticesArray = []

        for item in subgraphs:
            numberOfEdge = graphOperationsManager.calculateEdgeNumber(item,matrix)
            # try:
            #     metric[numberOfEdge]
            # except KeyError:
            #     metric[numberOfEdge] = {}
            # verticesDegrees = {}

            verticesDegreeSum = 0
            for vertice in item:
                verticeDegree = graphOperationsManager.calculateVerticeDegree(vertice, matrix)
                verticesDegreeSum += verticeDegree

                if(maxVerticeDegree < verticeDegree):
                    maxVerticeDegree = verticeDegree

                if(minVerticeDegree > verticeDegree):
                    minVerticeDegree = verticeDegree

            verticesArray.append(verticesDegreeSum/nodes)
                # try:
                #     verticesDegrees[verticeDegree] += 1
                # except KeyError:
                #     verticesDegrees[verticeDegree] = 1
            
            # verticesDegreesJSON = json.dumps(verticesDegrees)
            # try:
            #     metric[numberOfEdge][verticesDegreesJSON] += 1
            # except KeyError:
            #     metric[numberOfEdge][verticesDegreesJSON] = 1
                
        stepOneValue = minVerticeDegree + ((maxVerticeDegree-minVerticeDegree) / 3)
        stepTwoValue = stepOneValue + ((maxVerticeDegree-minVerticeDegree) / 3)

        for verticeDegree in verticesArray:
            if(verticeDegree > stepTwoValue):
                metric['high'] += 1
            elif (verticeDegree > stepOneValue):
                metric['medium'] += 1
            else:
                metric['low'] += 1

        return metric

    def prepare2kAnd3kSeriesMetric(self, matrix):
        metric = {}

        metric[2] = self.calculateDkSeries(matrix,2)
        metric[3] = self.calculateDkSeries(matrix,3)

        return metric