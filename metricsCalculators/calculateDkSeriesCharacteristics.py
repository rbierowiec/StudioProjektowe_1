#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import os
import sys
import inspect
import numpy as np

from metricsCalculators.graphOperations import graphOperations

class calculateDkSeriesCharacteristics:

    # Funkcja wyznacza charakterystyki dkSeries dla zadanej macierzy
    def calculateDkSeries(self, matrix, nodes, nodeDegrees):
        graphOperationsManager = graphOperations()
        subgraphs = graphOperationsManager.getAllNVerticesPaths(nodes, matrix, "*")

        characteristics = {}

        nodeOccurence = np.zeros(len(matrix[0]), dtype='int')
        
        # Wyliczenie ilości wystąpień każdego węzła (do policzenia dkSeriesSteps dla version = 2)
        for item in subgraphs:
            for vertice in item:
                nodeOccurence[vertice] += 1
        
        dkSeriesSteps = self.prepareSteps(nodeDegrees, nodeOccurence, 1)
        
        for item in subgraphs:
            characteristicsString = ""
            for vertice in item:
                if nodeDegrees[vertice] <= dkSeriesSteps[0]:
                    characteristicsString += "l_"
                elif nodeDegrees[vertice] <= dkSeriesSteps[1]:
                    characteristicsString += "m_"
                else:
                    characteristicsString += "h_"

            characteristicsString = characteristicsString[:-1]

            # Jeżeli nie istnieje klucz to go zdefiniuj
            try:
                characteristics[characteristicsString]
            except KeyError:
                characteristics[characteristicsString] = 1
            else:
                characteristics[characteristicsString] += 1

        return [characteristics, dkSeriesSteps]

    # Funkcja przygotowuje progi do wyznaczania low/medium/high w dkSeriesCharacteristics
    def prepareSteps(self, nodeDegrees, nodeOccurence, version):
        firstStep = -1
        secondStep = -1

        if version == 1:
            minNodeDegree = min(nodeDegrees)
            maxNodeDegree = max(nodeDegrees)
            
            firstStep = minNodeDegree + ((maxNodeDegree-minNodeDegree) / 3)
            secondStep = firstStep + ((maxNodeDegree-minNodeDegree) / 3)
        elif version == 2:
            degreesOccurence = {}

            index = 0
            for occurences in nodeOccurence:
                try:
                    degreesOccurence[nodeDegrees[index]]
                except KeyError:
                    degreesOccurence[nodeDegrees[index]] = occurences
                else:
                    degreesOccurence[nodeDegrees[index]] += occurences

                index += 1

            minNodeDegree = min(nodeDegrees)
            maxNodeDegree = max(nodeDegrees)

            # Wyznaczenie przedziałów na podstawie ilości wystąpień stopni węzła
            numberOfNodes = 0
            for key in degreesOccurence:
                numberOfNodes += degreesOccurence[key]
                if (numberOfNodes > sum(degreesOccurence.values()) / 3) and firstStep == -1:
                    firstStep = key
                elif (numberOfNodes >  sum(degreesOccurence.values()) / 3 * 2) and secondStep == -1:
                    secondStep = key

        return [firstStep, secondStep]

    def calculateCharacteristics(self, matrix, nodeDegrees = {}):
        
        if(len(nodeDegrees) == 0):
            nodesPassed = False
        else:
            nodesPassed = True

        graphOperationsManager = graphOperations()
        characteristics = {}
        
        characteristics["both"] = {}
        characteristics["inheritance"] = {}
        characteristics["association"] = {}

        matrixInheritance = np.zeros((2, len(matrix[0]), len(matrix[0])), dtype='int')
        matrixInheritance[0] = matrix[0]

        matrixAssociation = np.zeros((2, len(matrix[0]), len(matrix[0])), dtype='int')
        matrixAssociation[1] = matrix[1]

        tmpResultVariable = {}

        if not nodesPassed:
            # Wyliczenie stopnia dla poszczególnych węzłów
            nodeDegrees = np.zeros(len(matrix[0]), dtype='int')
            for i in range(len(matrix[0])):
                nodeDegrees[i] = graphOperationsManager.calculateVerticeDegree(i,matrix)

        # Policzenie charakterystyk dkSeries dla całej macierzy
        tmpResultVariable = self.calculateDkSeries(matrix, 2, nodeDegrees)
        characteristics["both"][2] = tmpResultVariable[0]

        tmpResultVariable = self.calculateDkSeries(matrix, 3, nodeDegrees)
        characteristics["both"][3] = tmpResultVariable[0]
        characteristics["both"]["steps"] = tmpResultVariable[1]


        if not nodesPassed:
            nodeDegrees = np.zeros(len(matrixInheritance[0]), dtype='int')
            for i in range(len(matrixInheritance[0])):
                nodeDegrees[i] = graphOperationsManager.calculateVerticeDegree(i,matrixInheritance)

        # Policzenie charakterystyk jedynie dla macierzy dziedziczenia
        tmpResultVariable = self.calculateDkSeries(matrixInheritance, 2, nodeDegrees)
        characteristics["inheritance"][2] = tmpResultVariable[0]

        tmpResultVariable = self.calculateDkSeries(matrixInheritance, 3, nodeDegrees)
        characteristics["inheritance"][3] = tmpResultVariable[0]
        characteristics["inheritance"]["steps"] = tmpResultVariable[1]



        if not nodesPassed:
            nodeDegrees = np.zeros(len(matrixAssociation[0]), dtype='int')
            for i in range(len(matrixAssociation[0])):
                nodeDegrees[i] = graphOperationsManager.calculateVerticeDegree(i,matrixAssociation)

        # Policzenie charakterystyk jedynie dla macierzy asocjacji
        tmpResultVariable = self.calculateDkSeries(matrixAssociation, 2, nodeDegrees)
        characteristics["association"][2] = tmpResultVariable[0]

        tmpResultVariable = self.calculateDkSeries(matrixAssociation, 3, nodeDegrees)
        characteristics["association"][3] = tmpResultVariable[0]
        characteristics["association"]["steps"] = tmpResultVariable[1]
        
        return characteristics