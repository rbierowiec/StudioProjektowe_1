#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random, os, sys, inspect, time
import numpy as np
import math, json, copy

dictionary = ['..\metricsCalculators']
for dict in dictionary:     
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], dict)))
    if cmd_subfolder not in sys.path:         
        sys.path.insert(0, cmd_subfolder)

from metricsManager import metricsManager
from calculateDkSeriesMetrics import calculateDkSeriesMetrics


# Class that holds all of the functions and modules needed in a genetic algorithm
class GeneticAlgorithm:

    #
    # Prepere all variables and create initial solutions
    #
    def __init__(self, solutions, nodes, numberOfAgregations, metrics, m2kAnd3kSeriesMetricPattern):
        print "Inicjalizacja"
        prepereArray, prepereArrayTypesNodes=[], []
        self.numberOfAgregationsArray = self.__probabilityNode(numberOfAgregations)
        for i in range(solutions):
            prepereArray.append([[np.random.choice([0, 1], [nodes,nodes], p=[0.8, 0.2]), np.random.choice(range(numberOfAgregations + 1), [nodes, nodes], p=self.numberOfAgregationsArray)]])
            prepereArrayTypesNodes.append(np.random.randint(0, 2, [1, nodes]))

        self.solutions = np.concatenate(prepereArray).astype("int_") # matrix of Connections
        self.connections = np.concatenate(prepereArrayTypesNodes).astype("int_") # matrix of types nodes
        self.fitness, self.fitnessALL, self.solutionsALL, self.connectionsALL = np.array([]), np.array([]), np.array([]), np.array([])
        self.pairs = []
        self.numberOfSolutions = solutions
        self.numberOfAgregations = numberOfAgregations
        self.bestSolution, self.bestConnections, self.bestFitness, self.ifSolutionIsTheSameArray = np.array([]), np.array([]), 1000000, np.zeros((solutions, 2), dtype=np.float)
        self.bestGenerationNumber = 0
        self.FoundBestSolution = False
        self.toVariationFitness = self.__countVariation(metrics)
        self.m2kAnd3kSeriesMetricPattern = m2kAnd3kSeriesMetricPattern
        self.choiceAgregationArray = np.arange(0, self.numberOfAgregations + 1)
        del prepereArray, prepereArrayTypesNodes
        print "Koniec Inicjalizacji"

    #
    # Create aggregation probability table
    #
    def __probabilityNode(self, numberOfAgregations):
        if numberOfAgregations == 0:   numberOfAgregationsArray = [1]
        elif numberOfAgregations == 1: numberOfAgregationsArray = [0.75, 0.25]
        elif numberOfAgregations == 2: numberOfAgregationsArray = [0.7, 0.2, 0.1]
        elif numberOfAgregations == 3: numberOfAgregationsArray = [0.5, 0.25, 0.15, 0.1]
        elif numberOfAgregations == 4: numberOfAgregationsArray = [0.45, 0.25, 0.15, 0.1, 0.05]
        return numberOfAgregationsArray

    #
    # Count denominator for fitness function
    #
    def __countVariation(self, metrics):
        denominator = 0
        for metric in metrics:
            if metrics[metric] > 0:
                if metric == "avarageNodeDegree":
                    for submetric in metrics[metric]:
                        if metrics[metric][submetric] > 0:
                            denominator += 1
                else:
                    denominator += 1
        return denominator

    #
    # Prepare pairs od solutions to crossover
    #
    def twoVec_to_crossover(self):
        self.pairs = []
        seq = range(self.numberOfSolutions)
        for i in range(0, int(self.numberOfSolutions / 2)):
            first = random.choice(seq)
            seq.pop(seq.index(first))
            second = random.choice(seq)
            seq.pop(seq.index(second))
            self.pairs.append([first, second])

    #
    # Crossover module - create new solutions
    # Type: Simple crossover
    #
    def crossover(self, typeCrossOver):
        solutionsTmp, connectionsTmp, ifTheSameArrayTmp= [], [], []
        lenS = len(self.solutions[0][0]) - 1
        for pair in self.pairs:
            if typeCrossOver == 'linear':
                x1 = random.randint(0, lenS)
                x2 = random.randint(0, lenS)
                y1 = random.randint(0, lenS)
                y2 = random.randint(0, lenS)

                if x1 > x2:
                    x1, x2 = x2, x1
                x2 += 1

                if y1 > y2:
                    y1, y2 = y2, y1
                y2 += 1

                copy1 = np.copy(self.solutions[pair[0]])
                copy2 = np.copy(self.solutions[pair[1]])

                copy_con1 = np.copy(self.connections[pair[0]])
                copy_con2 = np.copy(self.connections[pair[1]])

                for i in range(len(copy1)):
                    copy1[i, x1:x2, y1:y2], copy2[i, x1:x2, y1:y2] = copy2[i, x1:x2, y1:y2], copy1[i, x1:x2, y1:y2].copy()

                copy_con1[x1:x2], copy_con2[x1:x2] = copy_con2[x1:x2], copy_con1[x1:x2].copy()
                solutionsTmp.append([self.solutions[pair[0]]])
                solutionsTmp.append([self.solutions[pair[1]]])
                solutionsTmp.append([copy1])
                solutionsTmp.append([copy2])

                connectionsTmp.append([self.connections[pair[0]]])
                connectionsTmp.append([self.connections[pair[1]]])
                connectionsTmp.append([copy_con1])
                connectionsTmp.append([copy_con2])

                ifTheSameArrayTmp.append([self.ifSolutionIsTheSameArray[pair[0]]])
                ifTheSameArrayTmp.append([self.ifSolutionIsTheSameArray[pair[1]]])
                ifTheSameArrayTmp.append([[0,0]])
                ifTheSameArrayTmp.append([[0,0]])



        self.solutions = np.concatenate(solutionsTmp)
        self.connections = np.concatenate(connectionsTmp)
        self.ifSolutionIsTheSameArray = np.concatenate(ifTheSameArrayTmp)

    #
    # mutation module - chromosome modification
    # Two types:
    # 1) Uniform mutation
    # 2) Single swap mutation
    #
    def mutation(self, typeMutation, probability):
        lenS = len(self.solutions[0][0]) - 1
        index=-1
        for solution in self.solutions:
            index+=1
            if random.random() <= probability:
                if self.ifSolutionIsTheSameArray[index][0] == 1: self.ifSolutionIsTheSameArray[index][0] = 0
                if typeMutation == 1:  # Uniform mutation
                    for i in range(len(solution)):
                        x1 = random.randint(0, lenS)
                        x2 = random.randint(0, lenS)
                        y1 = random.randint(0, lenS)
                        y2 = random.randint(0, lenS)

                        if x1 > x2:
                            x1, x2 = x2, x1
                        x2 += 1

                        if y1 > y2:
                            y1, y2 = y2, y1
                        y2 += 1

                        rangeX = range(x1, x2)
                        rangeY = range(y1, y2)
                        if i == 0:
                            for X in rangeX:
                                for Y in rangeY:
                                    solution[i, X, Y] = random.randint(0, 1)

                        elif i == 1:
                            for X in rangeX:
                                for Y in rangeY:
                                    solution[i, X, Y] = np.random.choice(self.choiceAgregationArray,p=self.numberOfAgregationsArray)

                elif typeMutation == 2:  # Single swap mutation
                    for i in range(len(solution)):
                        x1a = random.randint(0, lenS)
                        x2a = random.randint(0, lenS)
                        y1a = random.randint(0, lenS)
                        y2a = random.randint(0, lenS)

                        xlen = abs(x1a-x2a)
                        ylen = abs(y1a-y2a)
                        if x1a > x2a:
                            x1a, x2a = x2a, x1a
                        x2a += 1

                        if y1a > y2a:
                            y1a, y2a = y2a, y1a
                        y2a += 1

                        if random.random() > 0.5:
                            x1b = random.randint(xlen, lenS)
                            x2b = x1b - xlen
                        else:
                            x1b = random.randint(0, lenS - xlen)
                            x2b = x1b + xlen

                        if random.random() > 0.5:
                            y1b = random.randint(ylen, lenS)
                            y2b = y1b - ylen
                        else:
                            y1b = random.randint(0, lenS - ylen)
                            y2b = y1b + ylen

                        if x1b > x2b:
                            x1b, x2b = x2b, x1b
                        x2b += 1

                        if y1b > y2b:
                            y1b, y2b = y2b, y1b
                        y2b += 1

                        solution[i, x1a:x2a, y1a:y2a], solution[i, x1b:x2b, y1b:y2b] = solution[i, x1b:x2b, y1b:y2b].copy(), solution[i, x1a:x2a, y1a:y2a].copy()

    #
    # Fitness module - Evaluation of all solutions
    #
    def fitnessFunction(self, metrics):
        self.fitness = np.array([])
        metricsManagerInstance = metricsManager()
        metricsManagerInstance2 = calculateDkSeriesMetrics()
        index = -1
        for solution in self.solutions:
            index += 1
            if self.ifSolutionIsTheSameArray[index][0] == 0: #jezeli nie byla zmieniana macierz to nie liczymy metryk
                result = 0
                metricsMatrix = metricsManagerInstance.calculateMetrics(solution, metrics)
                for metric in metrics:
                    if metrics[metric] > 0:
                        if metric == 'dkseries':
                            m2kAnd3kSeriesMetric = metricsManagerInstance2.prepare2kAnd3kSeriesMetric(solution)
                            for serie in self.m2kAnd3kSeriesMetricPattern:
                                for submetric in self.m2kAnd3kSeriesMetricPattern[serie]:
                                    result += math.pow(abs(
                                        self.m2kAnd3kSeriesMetricPattern[serie][submetric] -
                                        m2kAnd3kSeriesMetric[serie][submetric]), 2)

                        elif metric == 'avarageNodeDegree':
                            for submetric in metrics[metric]:
                                if metrics[metric][submetric] > 0:
                                    if submetric not in metricsMatrix[metric]:
                                        metricsMatrix[metric][submetric] = 0
                                    result += math.pow(abs(metricsMatrix[metric][submetric] - metrics[metric][submetric]), 2)
                        else:
                            result += math.pow(abs(metricsMatrix[metric] - metrics[metric]), 2)
                result = math.sqrt(result/self.toVariationFitness)
                self.fitness = np.append(self.fitness, result)
                self.ifSolutionIsTheSameArray[index][1] = result
                if result == 0:
                    self.FoundBestSolution = True
            else:
                self.fitness = np.append(self.fitness, self.ifSolutionIsTheSameArray[index][1])

    #
    # function for dkSeriesMetric
    #
    def dkseries(self):
        metricsManagerInstance = calculateDkSeriesMetrics()
        for soluction in self.solutions:
            m2kAnd3kSeriesMetric = metricsManagerInstance.prepare2kAnd3kSeriesMetric(soluction)
            self.bestSolution = soluction
            self.bestConnections = self.connections[0]
            return m2kAnd3kSeriesMetric

    #
    # Selection module - Choice of solutions to the next iteration
    # Two types:
    # 1) Elitism Selection
    # 2) Rank Selection
    #
    def selection(self, typeSelection):
        lenS = len(self.solutions)
        self.ifSolutionIsTheSameArray[:,0] = 1
        rangelenS=range(lenS)
        rangelenSminus1=range(lenS - 1)
        if typeSelection == 1:  # Elitism Selection
            for i in rangelenS:
                for j in rangelenSminus1:
                    if self.fitness[j] > self.fitness[j + 1]:
                        self.solutions[j + 1], self.solutions[j] = self.solutions[j], self.solutions[j + 1].copy()
                        self.connections[j + 1], self.connections[j] = self.connections[j], self.connections[j + 1].copy()
                        self.fitness[j + 1], self.fitness[j] = self.fitness[j], self.fitness[j + 1]
                        self.ifSolutionIsTheSameArray[j + 1], self.ifSolutionIsTheSameArray[j], = self.ifSolutionIsTheSameArray[j], self.ifSolutionIsTheSameArray[j + 1].copy()

            mid = lenS / 2
            self.solutions = self.solutions[:mid]
            self.connections = self.connections[:mid]
            self.ifSolutionIsTheSameArray = self.ifSolutionIsTheSameArray[:mid]

        elif typeSelection == 2:  # Rank Selection
            drawnedSolutionsArray, drawnedArray, drawnedFitnessArray, drawnedConnectionArray, drawnedifSolutionIsTheSameArray = [], [], [], [], []
            sumAdaptation, circle, getHalfSolutions = 0, 1, 1
            false_ = True
            for i in rangelenS:
                for j in rangelenSminus1:
                    if self.fitness[j] > self.fitness[j + 1]:
                        self.solutions[j + 1], self.solutions[j] = self.solutions[j], self.solutions[j + 1].copy()
                        self.connections[j + 1], self.connections[j] = self.connections[j], self.connections[j + 1].copy()
                        self.fitness[j + 1], self.fitness[j] = self.fitness[j], self.fitness[j + 1]
                        self.ifSolutionIsTheSameArray[j + 1], self.ifSolutionIsTheSameArray[j], = self.ifSolutionIsTheSameArray[j], self.ifSolutionIsTheSameArray[j + 1].copy()
            adaptationArray = list(range(lenS, 0, -1))
            for i in adaptationArray:
                sumAdaptation += i
            for i in range(len(adaptationArray)):
                adaptationArray[i] = float(adaptationArray[i]) / sumAdaptation
            for i in range(len(adaptationArray)):
                adaptationArray[i] = round(circle - adaptationArray[i], 8)
                circle = adaptationArray[i]
            while getHalfSolutions <= lenS // 2:
                randomNumber = random.random()
                j = 0
                for i in range(len(adaptationArray)):
                    if randomNumber >= adaptationArray[j]:
                        if i in drawnedArray:
                            false_ = False
                            break
                        drawnedArray.append(j)
                        drawnedSolutionsArray.append([self.solutions[j]])
                        drawnedFitnessArray.append([self.fitness[j]])
                        drawnedConnectionArray.append([self.connections[j]])
                        drawnedifSolutionIsTheSameArray.append([self.ifSolutionIsTheSameArray[j]])
                        break
                    j += 1
                if not false_:
                    false_ = True
                    continue
                getHalfSolutions += 1
            self.solutions = np.concatenate(drawnedSolutionsArray)
            self.fitness = np.concatenate(drawnedFitnessArray)
            self.connections = np.concatenate(drawnedConnectionArray)
            self.ifSolutionIsTheSameArray = np.concatenate(drawnedifSolutionIsTheSameArray)

    #
    # Function stores results
    #
    def storeBestSolution(self, iteration):
        if len(self.fitnessALL) != 0:
            self.fitnessALL = np.concatenate((self.fitnessALL, np.array([self.fitness])))
        else:
            self.fitnessALL = np.array([self.fitness])

        bestResult = min(self.fitness)
        if self.bestFitness > bestResult:
            x = np.argmin(bestResult)
            self.bestFitness = bestResult
            self.bestSolution = self.solutions[x]
            self.bestConnections = self.connections[x]
            self.bestGenerationNumber = iteration+1

    #
    # Function returns the best result
    #
    def returnBestResult2(self):
        return self.bestFitness, self.bestSolution, self.bestConnections, self.bestGenerationNumber, self.fitnessALL
