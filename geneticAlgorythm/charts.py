# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import math

class ChartsManager:

    def __init__(self, fitnessALL):
        length = 1+len(fitnessALL)
        step = length // 20
        if step == 0 : step +=1
        self.Xticks = range(1, length, step)
        if length-1 not in self.Xticks : self.Xticks.append(length-1)
        self.iterations = range(1,length)
        self.minArray, self.mediumArray, self.maxArray = [], [], []
        for fitnessArray in fitnessALL:
            self.minArray.append(np.min(fitnessArray))
            self.mediumArray.append(np.mean(fitnessArray))
            self.maxArray.append(np.max(fitnessArray))
        self.charts(self.minArray, self.mediumArray, self.maxArray, self.iterations)


    def charts(self, Y1, Y2, Y3, X):
        maxValue =  int(math.ceil(np.max(self.maxArray))+1)
        step2 = maxValue // 25
        if step2 == 0: step2 += 1
        Y=range(0, maxValue, step2)
        if maxValue-1 not in Y: Y.append(maxValue-1)
        #
        # plt.figure(1)
        # plt.xlabel('Iteracje',fontsize='large',fontweight='bold')
        # plt.ylabel('Funkcja celu',fontsize='large',fontweight='bold')
        # x,=plt.plot(X, Y1, 'g^', label=u'Najlepsza wartosc funkcji celu')
        # y,=plt.plot(X, Y2, 'b-', label=u'Srednia wartosc funkcji celu')
        # z,=plt.plot(X, Y3, 'rv', label=u'Najgorsza wartosc funkcji celu')
        # plt.xticks(X)
        # plt.yticks(Y)
        # plt.legend(handles=[x, y, z ], loc=0, fontsize=10)
        #
        # plt.figure(2)
        # plt.xlabel('Iteracje',fontsize='large',fontweight='bold')
        # plt.ylabel('Funkcja celu',fontsize='large',fontweight='bold')
        # x,= plt.plot(X, Y1, 'g--', label=u'Najlepsza wartosc funkcji celu')
        # y,=plt.plot(X, Y2, 'b-', label=u'Srednia wartosc funkcji celu')
        # z,=plt.plot(X, Y3, 'r-.', label=u'Najgorsza wartosc funkcji celu')
        # plt.xticks(X)
        # plt.yticks(Y)
        # plt.legend(handles=[x, y, z], loc=0, fontsize=10)

        plt.figure(3)
        plt.xlabel('Iteracje',fontsize='large',fontweight='bold')
        plt.ylabel('Funkcja celu',fontsize='large',fontweight='bold')
        x,=plt.plot(X, Y1, 'g-', label=u'Najlepsza wartosc funkcji celu')
        y,=plt.plot(X, Y2, 'b-', label=u'Srednia wartosc funkcji celu')
        z,=plt.plot(X, Y3, 'r-', label=u'Najgorsza wartosc funkcji celu')
        plt.xticks(self.Xticks)
        plt.yticks(Y)
        plt.legend(handles=[x, y, z], loc=0, fontsize=10)
        plt.show()

