# -*- coding: utf-8 -*-
import inspect
import os
import sys

dictionary = ['..\drowIGraph', '..\metricsCalculators', '..\geneticAlgorythm']
for dict in dictionary:     
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], dict)))
    if cmd_subfolder not in sys.path:         
        sys.path.insert(0, cmd_subfolder)

from classAlgorythmGenetic import GeneticAlgorithm
from charts import ChartsManager

# Class launch all modules in the genetic algorithm
class procesAlgorithm:
    def __init__(self, solutions, nodes, iterations, typeCrossover, typeMutation, propabilityMutation, typeSelection, numberOfAgregations, metrics, m2kAnd3kSeriesMetricPattern=""):
        self.__Sys = GeneticAlgorithm(solutions, nodes, numberOfAgregations, metrics, m2kAnd3kSeriesMetricPattern)

        for i in range(iterations):
            print 'Iteracja: ', i + 1
            if metrics['dkseries'] == 1:
                self.m2kAnd3kSeriesMetric = self.__Sys.dkseries()
            else:
                self.__Sys.twoVec_to_crossover()
                self.__Sys.crossover(typeCrossover)
                self.__Sys.mutation(typeMutation, propabilityMutation)
                self.__Sys.fitnessFunction(metrics)
                self.__Sys.storeBestSolution(i)
                self.__Sys.selection(typeSelection)
            if self.__Sys.FoundBestSolution:
                break
        print u"Zakończenie algorytmu"
        self.bestResult, self.bestSolution, self.bestConnection, self.generationNumber, self.fitnessALL = self.__Sys.returnBestResult2()
        if metrics['dkseries'] != 1:
            ChartsManager(self.fitnessALL)


if __name__=='__main__':
    import time
    from classMatrixToDraw import MatrixToDraw
    from metricsManager import metricsManager

    start_time = time.time()
    metrics = {}
    metrics['avarageNodeDegree'] = {2: 0, 3: 0}
    metrics['maxNodeDegree'] = 3
    metrics['maxSubgraphLength'] = 3
    metrics['medianDegree'] = 3
    metrics['dkseries'] = 0
    print "Skrypt start"
    A = procesAlgorithm(8, 15, 4, 'linear', 1, 0.5, 2, 2, metrics)
    print "Najlepszy rezultat:", A.bestResult
    print "Najlepszy matrix: \n", A.bestSolution
    print A.bestConnection
    print "Znaleziono w iteracji: ", A.generationNumber
    metricsManagerInstance = metricsManager()
    metricsMatrix = metricsManagerInstance.calculateMetrics(A.bestSolution, metrics)
    elapsed_time = time.time() - start_time

    for metric in metrics:
        if metrics[metric] != 0:
            if metric == 'avarageNodeDegree':
                for sumbetric in metrics[metric]:
                    if metrics[metric][sumbetric] != 0:
                        print metric, metrics[metric], metricsMatrix[metric]
            else:
                print metric, metrics[metric], metricsMatrix[metric]

    print 'wynik:', A.bestResult
    MatrixToDraw(A.bestSolution,A.bestConnection)
    elapsed_time = time.time() - start_time

