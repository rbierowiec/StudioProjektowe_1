# -*- coding: utf-8 -*-
import ontospy, numpy as np, sys, os, inspect, re
from drowIGraph.classMatrixToDraw import MatrixToDraw
from metricsCalculators import metricsManager
import sys


PROPERTIES = ['allValuesFrom','someValuesFrom','onClass']
DISPLAY = (sys.argv[2] == '1')
METRICS = (sys.argv[3] == '1')
FINDNEW = (sys.argv[4] == '1')

print "Options:"
print "DISPLAY:", DISPLAY
print "METRICS:", METRICS
print "FINDNEW:", FINDNEW

def getEquivalentClass(serialize):
    eqClasses=[]
    attributes=[]
    ecArray = re.findall(r'equivalentClass(.*?)\.', serialize, flags=re.DOTALL)
    serializeEdited = re.sub(r'equivalentClass(.*?)\.', '', serialize, flags=re.DOTALL)

    for ec in ecArray:
        for property in PROPERTIES:
            classesArray = re.findall(r''+property+' (.*?) [;|\]]', ec,flags=re.DOTALL)
            for classe in classesArray:
                className = re.sub(r'\W+', '', classe)
                if className not in classesNamesArray:
                    attributes.append(className)
                else:
                    eqClasses.append(className)
    return eqClasses,attributes,serializeEdited

def getSubClass(serialize):
    attributes=[]
    scClasses=[]
    scArray = [serialize]
    for sc in scArray:
        for property in PROPERTIES:
            classesArray = re.findall(r'' + property + ' (.*?) [;|\]]', sc, flags=re.DOTALL)
            for classe in classesArray:
                className = re.sub(r'\W+', '', classe)
                if className not in classesNamesArray:
                    attributes.append(className)
                else:
                    scClasses.append(className)
    return scClasses, attributes

try:
    model = ontospy.Ontospy(str(sys.argv[1]))
    if model.classes == []:
        raise Exception()
except:
    sys.exit("Error. Podana ścieżka nie istnieje lub nie zawiera poprawnego pliku OWL")


classesMatrix = []
instancesMatrix = []
classesNamesArray=[]
objectsMap={}
objectsProperties={}
instancesNames=[]
propertynames=[]

for pop in model.properties:
    propertynames.append(pop.bestLabel())
nodeType=[]
index = 0
for classModel in model.classes: #zebranie wszystkli klas i instancji aby mozna bylo stworzyć macierz.
    objectsMap[classModel.bestLabel()] = index
    classesNamesArray.append(classModel.bestLabel())
    nodeType.append(0)
    index += 1
    for instance in classModel.instances():
        nameinstance = instance.lower().rsplit("#", 1)[-1]
        if(instance not in instancesNames):

            instancesNames.append(nameinstance)

        objectsMap[nameinstance] = index
        nodeType.append(1)
        index += 1
# Zebranie nazw property
# for classProperty in model.properties:  # zebranie property
#     objectsMap[classProperty.bestLabel()] = index
#     nodeType.append(2)
#     index += 1

onto = np.zeros((2, len(objectsMap), len(objectsMap)), dtype='int')

for classModel in model.classes:
    # stworzenie dziedziczenia po klasach i instancjach

    nameClass = classModel.bestLabel()
    objectsProperties[objectsMap[nameClass]] = {}
    objectsProperties[objectsMap[nameClass]]['eq'] = {}
    objectsProperties[objectsMap[nameClass]]['sc'] = {}

    #dziedziczenia po klasie
    for parent in classModel.parents():
        onto[0][objectsMap[nameClass]][objectsMap[parent.bestLabel()]]=1

    #dziedziczenia klasa<-instancja
    for instance in classModel.instances():
        onto[0][objectsMap[instance.lower().rsplit("#", 1)[-1]]][objectsMap[nameClass]] = 1

    serialize = classModel.serialize()
    eqArrayClasses,attributesEq,serializeEdited = getEquivalentClass(serialize) # obie linijki brane są jako assocjacje
    sbArrayClasses,attributesSc = getSubClass(serializeEdited) # obie linijki brane są jako assocjacje

    #Asocjacje
    for classe in eqArrayClasses:
        onto[1][objectsMap[nameClass]][objectsMap[classe]] += 1
    for classe in sbArrayClasses:
        onto[1][objectsMap[nameClass]][objectsMap[classe]] += 1

    # Atrybuty:
    for attribute in attributesEq: #eq - equivalent - assocjacje
        try:
            objectsProperties[objectsMap[nameClass]]['eq'][attribute] += 1
        except:
            objectsProperties[objectsMap[nameClass]]['eq'][attribute] = 1
    for attribute in attributesSc: # subclass - dziedziczenia
        try:
            objectsProperties[objectsMap[nameClass]]['sc'][attribute] += 1
        except:
            objectsProperties[objectsMap[nameClass]]['sc'][attribute] = 1


modelSerialize = model.serialize()
for instance in instancesNames:
    instanceArray = re.findall(r''+instance+'(.*?)\.', modelSerialize, flags=re.DOTALL)
    for row in instanceArray:
        for property in propertynames:
            instanceAsociacion = re.findall(r'' + property + '(.*?);', row, flags=re.DOTALL)
            for rowAs in instanceAsociacion:
                instanceName = re.sub(r'\W+', '', rowAs)
                onto[1][objectsMap[instanceName]][objectsMap[instance]] +=1





# dziedziczenie po propertach
# for properiesModel in model.properties:
#     for parentProperty in properiesModel.parents():
#         onto[0][objectsMap[properiesModel.bestLabel()]][objectsMap[parentProperty.bestLabel()]] = 1


if METRICS:
    import json
    import codecs
    from metricsCalculators.calculateDkSeriesMetrics import calculateDkSeriesMetrics
    from metricsCalculators.calculateDkSeriesCharacteristics import calculateDkSeriesCharacteristics
    from metricsCalculators.objectPropertiesNumberCalculator import objectPropertiesNumberCalculator
    metrics = {}
    metrics['maxNodeDegree'] = 1
    metrics['maxSubgraphLength'] = 1
    metrics['medianDegree'] = 1
    metrics['avarageNodeDegree'] = {}
    metrics['avarageNodeDegree'][2] = 1
    metrics['avarageNodeDegree'][3] = 1


    metricsManagerInstance = calculateDkSeriesMetrics()
    metricsManagerInstanceX = metricsManager.metricsManager()
    metricsManagerInstance2 = calculateDkSeriesCharacteristics()
    metricsManagerInstance3 = objectPropertiesNumberCalculator()

    m2kAnd3kSeriesMetric = metricsManagerInstance.prepare2kAnd3kSeriesMetric(onto)
    metricsMatrix = metricsManagerInstanceX.calculateMetrics(onto,metrics)
    metricsMatrix['dkseriesSectors']=metricsManagerInstance2.calculateCharacteristics(onto)
    metricsMatrix['objectProperties'] = metricsManagerInstance3.calculateObjectPropertiesNumber(objectsProperties,len(onto[0])).tolist()
    metricsMatrix["dkseries"] = m2kAnd3kSeriesMetric

    print "Zapisano wyniki metryk w plikach output.json oraz output.csv"
    wyjscie = open("output.json", "w")
    jsonExport=json.dumps(metricsMatrix)
    wyjscie.write(jsonExport)

    f = codecs.open(u"output.csv", encoding='cp1250', mode="wb")
    f.write(u"Nazwa metryki;Wartość metryki;Kodowanie windows-1250\n")
    for metric in metricsMatrix:
        f.write(str(metric)+";" + str(metricsMatrix[metric]).replace('.',',')+ "\n")


if DISPLAY:
    MatrixToDraw(onto, nodeType)

if FINDNEW:
    from Interface.InterfaceFunction import Interface
    from geneticAlgorythm.geneticManager import procesAlgorithm
    from metricsCalculators.calculateDkSeriesMetrics import calculateDkSeriesMetrics

    interfaceClass = Interface()
    parentsNumber, nodeNumber, iterationNumber, mutationType, mutationProbability, selectionType, agregations,  null, null, null, null, null = interfaceClass.fileInterface('Program2')
    metricsx = {}
    metricsx['dkseries'] = 2

    B = procesAlgorithm(parentsNumber, nodeNumber, iterationNumber, 'linear', mutationType, mutationProbability,
                        selectionType, agregations, metricsx, m2kAnd3kSeriesMetric)

    #####    Results    #####
    print "Najlepszy rezultat:", B.bestResult
    print "Najlepszy matrix: \n", B.bestSolution
    print B.bestConnection
    print "Znaleziono w iteracji: ", B.generationNumber
    metricsManagerInstance = calculateDkSeriesMetrics()
    m2kAnd3kSeriesMetric = metricsManagerInstance.prepare2kAnd3kSeriesMetric(B.bestSolution)
    print m2kAnd3kSeriesMetric

    MatrixToDraw(B.bestSolution, B.bestConnection)