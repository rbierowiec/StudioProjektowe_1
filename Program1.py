# -*- coding: utf-8 -*-
from Interface.InterfaceFunction import Interface
from drowIGraph import classMatrixToDraw
from geneticAlgorythm.geneticManager import procesAlgorithm
from metricsCalculators import metricsManager

if __name__=='__main__':

    ##### Interface start #####
    print u'Wybierz interfejs \n1 - wartości z pliku\n2 - wartości ręczne'

    while True:
         interfacenr = raw_input()
         if interfacenr in ('1','2'): break
         print "Liczba błędna"

    interfaceClass = Interface()
    if interfacenr == '2':
        parentsNumber, nodeNumber, iterationNumber, mutationType, mutationProbability, selectionType, agregations = interfaceClass.TerminalInterface()
        print u"Podaj wartości metryk. 0 - metryka wyłaczona \n" \
              " \n" \
              "maxNodeDegree \n" \
              "maxSubgraphLength \n" \
              "medianDegree \n" \
              "avarageNodeDegree 2-nodes \n" \
              "avarageNodeDegree 3-nodes"

        metrics = {}
        metrics['maxNodeDegree'] = int(raw_input())
        metrics['maxSubgraphLength'] = int(raw_input())
        metrics['medianDegree'] = int(raw_input())
        metrics['avarageNodeDegree'] = {}
        metrics['avarageNodeDegree'][2] = int(raw_input())
        metrics['avarageNodeDegree'][3] = int(raw_input())
        print metrics

        metrics['dkseries'] = 0

        ##### Program start #####

    elif interfacenr == '1':
        metrics = {}
        metrics['avarageNodeDegree'] = {}
        parentsNumber, nodeNumber, iterationNumber, mutationType, mutationProbability, selectionType, agregations, metrics['maxNodeDegree'], metrics['maxSubgraphLength'], metrics['medianDegree'], metrics['avarageNodeDegree'][2], metrics['avarageNodeDegree'][3] = interfaceClass.fileInterface('Program1')
        print metrics
        metrics['dkseries'] = 0


    A = procesAlgorithm(parentsNumber, nodeNumber, iterationNumber, 'linear', mutationType, mutationProbability, selectionType, agregations, metrics)

    #####    Results    #####

    print "Najlepszy rezultat:", A.bestResult
    print "Najlepsza macierz: \n", A.bestSolution
    print A.bestConnection
    print "Znaleziono w iteracji: ", A.generationNumber
    metricsManagerInstance = metricsManager.metricsManager()
    metricsMatrix = metricsManagerInstance.calculateMetrics(A.bestSolution, metrics)

    for metric in metrics:
        if metrics[metric] != 0:
            if metric == 'avarageNodeDegree':
                for sumbetric in metrics[metric]:
                    if metrics[metric][sumbetric] != 0:
                        print metric, metrics[metric], metricsMatrix[metric]
            else:
                print metric, metrics[metric], metricsMatrix[metric]

    print 'wynik:', A.bestResult
    classMatrixToDraw.MatrixToDraw(A.bestSolution, A.bestConnection)

    print u"Po zapisaniu pliku wprowadź dowolny znak"
    x = raw_input()